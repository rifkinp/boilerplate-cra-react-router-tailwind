import React from "react";

const Home = () => {
  return (
    <div className="flex items-center justify-center text-center w-full h-screen bg-emerald-200">
      <div>Create-React-App, React Router & Tailwind BoilerPlate</div>
    </div>
  );
};

export default Home;
